#ifndef LIBRARIES_LBR_THREADS_H_
#define LIBRARIES_LBR_THREADS_H_

/******************************************************************************
 * Includes
 ******************************************************************************/

#include <string.h>
#include <inttypes.h>

/*
 * 	@info: STM Third Party Libraries
 *	@path: Middlewares/Third_Party/FreeRTOS/Source/Include
 */
#include <FreeRTOS.h>
#include <task.h>
#include <cmsis_os.h>

#include "lbr_framework.h"
#include "Inc/stm32f779i_eval.h"


/******************************************************************************
 * Definitions & Declarations
 ******************************************************************************/

typedef enum
{
	TT_PROCEDURE	= 0x00,
	TT_APPLICATION	= 0x01
}THREAD_TYPE;

typedef enum
{
	TC_RUNNING 		= 0x00,
	TC_WAITING		= 0x01
}THREAD_CONDITION;

typedef enum
{
	TS_INACTIVE 	= 0x00,
	TS_INITIALIZE 	= 0x01,
	TS_ACTIVE 		= 0x02,
	TS_DEINITIALIZE = 0x03,
	TS_ERROR_ALL	= 0x04
}THREAD_STATE;

typedef enum
{
	TM_AUTO 		= 0x00,
	TM_MANUAL		= 0x01
}THREAD_MODE;

typedef struct
{
	THREAD_TYPE 		type;
	THREAD_STATE 		state;
	THREAD_CONDITION 	condition;
	THREAD_MODE			mode;
}
thread_info_t;

/******************************************************************************
 * Global Variables
 ******************************************************************************/

/******************************************************************************
 * Functions
 ******************************************************************************/

/*
 * @func   isca_framework_thread_init
 * @brief  <missing>
 * @param  <missing>
 * @retval void
 */

void lbr_thread_init(thread_info_t *, THREAD_TYPE, THREAD_MODE);

/*
 * @func   isca_framework_thread_deinit
 * @brief  <missing>
 * @param  <missing>
 * @retval void
 */

void lbr_thread_deinit(thread_info_t *, osThreadId *);
/*
 * @func   isca_framework_thread_execute
 * @brief  <missing>
 * @param  <missing>
 * @retval void
 */

void lbr_thread_execute(thread_info_t *);

/*
 * @func   isca_framework_thread_deinit
 * @brief  <missing>
 * @param  <missing>
 * @retval void
 */

uint8_t lbr_thread_start(char * name, void (*functionPtr)(const void *), thread_info_t * thread_info, osThreadId * thread_handle, const void * arguments);

/*
 * @func   isca_framework_thread_deinit
 * @brief  <missing>
 * @param  <missing>
 * @retval void
 */

uint8_t lbr_thread_toggle(char * name, void (*functionPtr)(const void *), thread_info_t * thread_info,	osThreadId * thread_handle,void *);

/*
 * @func   isca_framework_thread_barrier
 * @brief  <missing>
 * @param  <missing>
 * @retval void
 */

uint8_t lbr_thread_stop(thread_info_t * thread_info);

/*
 * @func   isca_framework_thread_barrier
 * @brief  <missing>
 * @param  <missing>
 * @retval void
 */
void lbr_thread_barrier(thread_info_t *, THREAD_STATE);

#endif /* LBR_THREADS_H_ */
