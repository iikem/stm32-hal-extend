/******************************************************************************
 * Source File Start
 ******************************************************************************/

/******************************************************************************
 * Includes
 ******************************************************************************/

#include "ta_echo_can.h"

/******************************************************************************
 * Global Variables
 ******************************************************************************/

	osThreadId 		ta_echo_can1_handle;
	thread_info_t  	ta_echo_can1_info;

	osThreadId 		ta_echo_can3_handle;
	thread_info_t  	ta_echo_can3_info;

/******************************************************************************
 * Functions
 ******************************************************************************/

void dev_can1_rx_callback()
{
	dev_can_send_frame(CAN_1,dev_can_get_rx_frame(CAN_1));
}

void dev_can3_rx_callback()
{
	dev_can_send_frame(CAN_3,dev_can_get_rx_frame(CAN_3));
}

void ta_echo_can(void const * arguments)
{
	uint8_t selected_can = *((uint8_t *)arguments);

	lbr_thread_init(selected_can == 0 ? &ta_echo_can1_info : &ta_echo_can3_info, TT_APPLICATION, TM_MANUAL);

	if( selected_can == 0 )
	{
		if(dev_can_init(CAN_1)==I_INVALID)
		{
			ta_echo_can1_info.state = TS_ERROR_ALL;
			osDelay(345);
			lbr_thread_deinit(&ta_echo_can1_info, &ta_echo_can1_handle);
			return;
		}
		dev_can_set_rx_callback(CAN_1,dev_can1_rx_callback);
	}

	if( selected_can == 3 )
	{
		if(dev_can_init(CAN_3)==I_INVALID)
		{
			ta_echo_can3_info.state = TS_ERROR_ALL;
			osDelay(345);
			lbr_thread_deinit(&ta_echo_can3_info, &ta_echo_can3_handle);
			return;
		}
		dev_can_set_rx_callback(CAN_3,dev_can3_rx_callback);
	}

	osDelay(10);

	// Main Loop

	lbr_thread_execute(selected_can == 0 ? &ta_echo_can1_info : &ta_echo_can3_info);

	while ((selected_can == 0 ? ta_echo_can1_info.state : ta_echo_can3_info.state) == TS_ACTIVE)
	{
		osDelay(128);
	}

	// DeInitialize

	if( selected_can == 0 )
		dev_can_deinit(CAN_1);
	if(  selected_can == 3 )
		dev_can_deinit(CAN_3);

	lbr_thread_deinit(selected_can == 0 ? &ta_echo_can1_info : &ta_echo_can3_info, selected_can == 0 ? &ta_echo_can1_handle : &ta_echo_can3_handle);
}

/******************************************************************************
 * Source File End
 ******************************************************************************/
