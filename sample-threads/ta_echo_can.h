#ifndef THREADS_TA_ECHO_CAN_H_
#define THREADS_TA_ECHO_CAN_H_

/******************************************************************************
 * Includes
 ******************************************************************************/

	#include <string.h>
	#include <inttypes.h>

	// CubeMX Libraries
	#include <cmsis_os.h>
	#include "FreeRTOS.h"
	#include "Drivers/BSP/Inc/stm32f779i_eval.h"
	#include "rng.h"

	#include <Libraries/lbr_framework.h>
	#include <Libraries/lbr_threads.h>

	#include <Devices/dev_can.h>

/******************************************************************************
 *  Structures & Global Variables
 ******************************************************************************/

	extern osThreadId 		ta_echo_can1_handle;
	extern thread_info_t  	ta_echo_can1_info;

	extern osThreadId 		ta_echo_can3_handle;
	extern thread_info_t  	ta_echo_can3_info;

	extern uint8_t 	ta_echo_can_port;
	extern uint32_t ta_echo_latency_ms;
/******************************************************************************
 * Functions
 ******************************************************************************/

	void ta_echo_can(void const * arguments);

/******************************************************************************
 * Header File Guard Symbol End
 ******************************************************************************/


#endif /* THREADS_TA_ECHO_CAN_H_ */
